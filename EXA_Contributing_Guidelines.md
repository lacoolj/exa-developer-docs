# EXA Best Practices Guide/Contributing Guidelines

Developers should refer to this guide on the best practices we like to use in EXA. 
***

## Goals

The over arching goals of EXA Best practices guide will be to come up with a common
set of expectations code style/quality that all developers can follow. Down the line,
we can enforce this with a tool such as eslint.

Code following this guide will be naturally self documenting as well as explicitly
documented. Functions will have clear arguments and their return values as well
as flow will be predictable and easy to comprehend.

We also want the code to be easily updateable in the future. These practices will help ensure
that down the line we won't be tied to a piece of code because of how hard it is to
untangle.

## Data Layer Best Practices

This is for the back end of EXA - specifically functions that directly talk to the database

* #### Prefer `const`, occasionally use `let`, **never** use `var`
    There is never a good reason to use var on the back end of EXA. `let` and `const`
    should be the way we declare variables on the backend. They provide an extra layer of
    protection from confusing scoping rules and data mutation that could be prone to bugs.

    * Objects should *always* be declared with `const` because you can write new properties
    onto the object without mutating it.

* #### Prefer Arrow Functions for Callbacks
    Developers should opt for arrow function syntax for all callback functions except in rare occasions that
    the `this` binding specifically calls for the use of regular Functions

* #### Prefer Regular Functions for Methods
    This is already happening in most of EXA. This will ensure that
    the expected `this` context will be there when needed.

* #### Destructure required values for every function
    Destructuring required values for every function helps make the code much simpler
    to read documenting the needed arguments in each incoming object as well making
    functions easier to read by having all the variables exist by themselves.

    * Destructuring can be done within the parameters of the function.
    * Destructuring can occur within the function as well and the original argument is preserved.
    * DO NOT destructure unused variables. This defeats some of the purpose of destructuring clarity.

* #### Always Define SQL Queries as variables
    By pulling out all query statements into variables - this makes the functions substantially easier to read.

* #### Prefer using `sql-template-strings`
    sql-template-strings is an amazing package for easily parameterizing functions
    as well as creating very well self documented code

* #### Always use strict equality
    We should always use `===` whenever we can. On the rare occasion we need coercion
    to occur that should be clearly documented with a comment.

* #### Always return callbacks
    V8 can do a number of performance optimizations when a callback is returned as
    well as ensure a callback never gets called more than once. When using arrow functions,
    you can immediately return a value by excluding the brackets without the use of the
    return keyword.

* #### All new functions should be documented with [JSDoc]('http://usejsdoc.org/').
    This will help further document our codebase and make development easier, faster,
    and less prone to bugs. Most important document we want is the [@param](http://usejsdoc.org/tags-param.html)

* #### Always refactor and remove Async.io
    All instances of the async package should be removed as they are found. If there
    is specific need for flow control, functions should return native JavaScript promises and async/await
    syntax can provide clear and concise flow control for the function.

* #### Avoid large if/else blocks
    We should strive to keep the logical flow of the application obvious at every point.
    Many functions currently use large nested if/else blocks that call different logical
    patterns whenever certain conditions are encountered. We should keep this to a minimum,
    pull out functions that perform logical operations into their own helpers.

* #### Use Verbose and correct function and variable names with correct spelling
    All function and variable names should be clear exactly what we expect the function to
    do and what information the variable should contain. Variables should also be spelled correctly.
    All JavaScript variables should be camelCased. SQL table and column names should be snake_cased.

    * Booleans should be prefixed with 'is' or 'has.' ie... isActive or hasPatients
    * Arrays should always be plural and imply many
    * Objects should imply  many and also what data they are representing... Bad variable name is `args`.
    Good variable name is `finalIncomingPatientSaveData`.


Below is a small example of how many back end functions should look after you touch them.
```javascript
    const SQL = require('sql-template-strings');
    {...

        // Here we destructure all the values that this function needs as the
        // arguments are passed in - we could have also destructured within the function.

        /** functionName is an example of a good function
         *  @param {object} functionArgs - incoming data
         *  @param {string} functionArgs.arg1 - some string we need
         *  @param {string} functionArgs.arg3.arg4 - some data we need to filter on
         */
        functionName: ({
            arg1,
            arg3: {
                arg4,
            } = {}
        },
        callback) => {

            // Always declare sql queries as variables. This unties the query from the function.
            // It makes the string easier to read as it stands by itself.
            // We also use sql-template-strings to help parameterize the query and
            // further make it easier to read and comprehend.
            // We can always use const when using sql-template-strings since sql is an object
            const sql = SQL`
                SELECT
                    some_row
                FROM
                    some_table
                WHERE
                    some_data = ${arg4}
            `;

            // we can generally condense our db call to one easy to read line
            // notice we use an arrow function as a callback and destructure the rows
            // property. This makes for a terse but easy to understand syntax.
            // We should always avoid adding more logic within the callback.
            helper.query(sql.text, sql.params, (err, {rows}) => callback(err, rows));
        },...
    }
```
